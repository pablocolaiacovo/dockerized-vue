# Dockerized VUE

[https://dockerized-vue.azurewebsites.net/](https://dockerized-vue.azurewebsites.net/)

- [x] Dockerfile, `docker build` & `docker run`
- [x] Connect to Gitlab Pipeline
- [x] Deploy somewhere
- [ ] GraphQL Apollo configuration
- [ ] Deploy it with GraphQL + Apollo

## Docker Image

```sh
$ docker build -t dockerized-vue .
$ docker run -it -p 8080:80 --rm --name dockerized-vue dockerized-vue
```

Reference: https://vuejs.org/v2/cookbook/dockerize-vuejs-app.html

## Registry

With `az-cli`

```sh
$ az login
$ az acr login --name colaiacovo
$ az acr show --name colaiacovo --query loginServer --output table
$ docker tag dockerized-vue colaiacovo.azurecr.io/dockerized-vue
$ docker push colaiacovo.azurecr.io/dockerized-vue
```

With `docker`

```sh
$ echo "password" | docker login registry.url -u username --password-stdin
$ docker tag dockerized-vue colaiacovo.azurecr.io/dockerized-vue
$ docker push colaiacovo.azurecr.io/dockerized-vue
```

## GraphQL Apollo

Start the GraphQL API Server

```sh
$ yarn apollo
```

## Pipeline

Install: https://docs.gitlab.com/runner/install/

Test steps:

```sh
$ gitlab-runner exec docker step-to-test
```

More executors: https://docs.gitlab.com/runner/executors/README.html
